class openedge:
    define public property Prop1 as character no-undo
        get.
        private set.
        
    constructor public openedge(input cProp as character):
        this-object:Prop1 = cProp.
    end constructor.
    
    method public character ValueOf():
        return Prop1 + " - was the value".
    end method.

end class.
